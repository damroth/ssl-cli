[![pipeline status](https://gitlab.com/damroth/ssl-cli/badges/master/pipeline.svg)](https://gitlab.com/damroth/ssl-cli/-/commits/master)
# SSL CLI

A simple script with which you can easily verify the SSL certificate.

`-s` will return information whether a certificate is valid or not

`-c` will pull remote SSL certificate and print out some info about it

`-h` displays help message and banner

`ruby scli.rb -s google.com -> [✓] Certificate is valid`

`ruby scli.rb -g google.com -> 
#<OpenSSL::X509::Certificate: subject=#<OpenSSL::X509::Name CN=*.google.com,O=Google LLC,L=Mountain View,ST=California,C=US>, issuer=#<OpenSSL::X509::Name CN=GTS CA 1O1,O=Google Trust Services,C=US>, serial=#<OpenSSL::BN:0x0000000002687ed0>, not_before=2020-04-28 07:43:41 UTC, not_after=2020-07-21 07:43:41 UTC>`

**works best with ruby 2.6**