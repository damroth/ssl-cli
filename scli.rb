#!/usr/bin/env ruby
require 'tty' 
require 'slop'
require 'net/http'
require 'openssl'

CHKM = "[\u2713]"
XMRK = "[\u2717]"

def verify_certificate(hostname)
    if (!hostname.include?("https") || !hostname.include?("http"))
        uri = URI("https://" + hostname)
    else
        uri = URI(hostname)
    end
    begin
        @res = Net::HTTP.get_response(uri)
        puts CHKM + " Certificate is valid"
    rescue OpenSSL::SSL::SSLError
        byebug
        cert = get_remote_cert(hostname)
        if cert.not_after < Time.now.utc
            puts XMRK + " Certificate is out of date: #{cert.not_after}"
        else
            puts XMRK + " Certificate is inavlid: #{cert.inspect}"
        end
    rescue Errno::ECONNREFUSED
        puts XMRK + " Cannot connect, server refusing connection on port 443"
    rescue SocketError
        puts XMRK + " Connot connect to host, maybe domain name is invalid or server is down"
    end
end

def get_remote_cert(hostname)
    if (hostname.include?("https") || hostname.include?("http"))
        raw_hostname = hostname.sub(/https?:\/\//, '')
    end
    begin
        sock = TCPSocket.new(raw_hostname || hostname, 443)
        ctx = OpenSSL::SSL::SSLContext.new
        ctx.set_params(verify_mode: OpenSSL::SSL::VERIFY_NONE)
        socket = OpenSSL::SSL::SSLSocket.new(sock, ctx)
        socket.hostname = raw_hostname || hostname
        socket.sync_close = true
        socket.connect
        return socket.peer_cert
    rescue
        puts XMRK + " Cannot connect, server refusing connection on port 443"
        exit(false)
    end
end

def display_banner
    font = TTY::Font.new(:starwars)
    puts font.write("ssl cli")
end

opts = Slop.parse do |o|
    o.string "-s", "--ssl", "[domain] Check if certificate is valid for given domain"
    o.string "-g", "--get", "[domain] Get and display remote certificate for given domain"
    o.on "-h", "--help", "help"
end

if opts[:ssl]
    verify_certificate(opts[:ssl])
elsif opts[:get]
    puts get_remote_cert(opts[:get]).inspect
elsif opts[:help]
    display_banner()
    puts opts
else
    display_banner()
    puts opts 
end